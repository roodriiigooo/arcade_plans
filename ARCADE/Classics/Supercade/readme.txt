Supercade Design
Copyright Jeffrey Allen (c) 2003

Important!!! Read the following if you plan on using these plans! I really mean it!

I'm getting a lot of requests for my plans. Here's the problem: I didn't use any. At least not the kind that you can follow and build your own from. What I did use was a set of png files that show the side view of the entire machine, and a drawing of the control panel from above. I figured out the exact dimensions as I went.

I do not recommend downloading these files and using them as a "written in stone" template. Instead, use them as a guide for designing your own. You are going to have your own needs which will require modifying these diagrams anyway. But if you need a starting point, then I give you these. It's what I used.

A couple of important points:

- The png files were designed in Macromedia Fireworks. If you have access to this program, then I recommend using it.

- In the png files, 1 inch = 16 pixels. You can use this scale to figure out most of the measurements.

- I built the control panel first, I recommend doing the same, but use your own judgement.

- The control panel shown in the diagram doesn't match what you see in the photos on this site. The drawing is right, not the photos. After I finished building the machine, I decided to fine-tune the control panel, so it was modified (yes, this meant rebuilding large portions of it). I don't have a digital camera handy, so I haven't taken new photos.

- I used a 27" arcade monitor. You should take careful measurements of whatever monitor you are using, and modify the design appropriately.

- I used 3/4" plywood on the machine. This is important for the measurements. If you use something else, you'll need to adjust the dimensions appropriately.

- There is 27" in between the two sides of the main machine. In other words, the inside width of the machine is 27", the outside width (not including the control panel) is 27" + 3/4" + 3/4" = 28.5". The control panel is much larger (37" at its widest).

- For figuring out what measurements to use for building the base of the control panel, you're on your own. I hope you took trig.

- These plans come with no guarantees or promises. It is my personal hope that you will not try to duplicate my machine, but rather that you will use my design as a starting point to come up with something even better. If you follow my plans and you spend a bunch of money, and it doesn't work, you're on your own. I accept no liability, and by downloading these plans, you agree that I am not responsible for what you do with them. If you download these plans and build something even remotely similar, please send me pictures of my grandchildren.
