# Arcade / Pinball
Repositório criado com o intuito de manter uma coleção de planos de montagem, templates e manuais para dar uma direção, inspiração ou apenas colecionar coisas sobre o tema.
Ajude a manter, organizar ou contribuir com o repositório entrando em contato comigo ou fazendo pull requests!

Os arquivos deste repositório foram reunidos diretamente de downloads da internet ou por contribuições de terceiros, se encontrar algo de sua autoria e deseja que seja removido, basta entrar em contato comigo.

contato@rodrigo.londrina.br
#

